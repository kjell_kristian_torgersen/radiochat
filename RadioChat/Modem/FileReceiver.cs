﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioChat.Modem
{
    class FileReceiver
    {
      
        HashSet<ushort> receivedBlocks;
        List<ushort> missing;
        BinaryWriter writer = null;
        Controller controller;
        String filename;
        public FileReceiver(Controller controller) {
            receivedBlocks = new HashSet<ushort>();
            missing = new List<ushort>();
            this.controller = controller;
        }

        public void PackageReceived(Package package) {
            switch (package.Type)
            {
                case PackageType.FileBlock:
                    int idx = package.Payload[0] + (package.Payload[1] << 8);
                    if (writer == null)
                    {
                        string fileName = DateTime.Now.ToString("yyyy-MM-dd_HHmm.bin");
                        writer = new BinaryWriter(File.Open(fileName, FileMode.Create));
                    }
                    writer.Seek(192 * idx, SeekOrigin.Begin);
                    writer.Write(package.Payload, 2, package.Payload.Length - 2);
                    break;
                case PackageType.FileSummary:
                    int packages = package.PayloadShort[0];
                    filename = "";
                    for (int i = 2; i < package.Payload.Length; i++) {
                        filename += (char)package.Payload[i];
                    }
                    for (int i = 0; i < packages; i++) {
                        if (!receivedBlocks.Contains(i)) {
                            missing.Add(i);
                        }
                    }

                    Package missingPackages = new Package(PackageType.FileMissingBlocks, missing.ToArray());
                    controller.SendPackage(missingPackages);
                    break;
            }
            }
    }
}
