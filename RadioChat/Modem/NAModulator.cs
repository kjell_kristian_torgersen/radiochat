﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioChat
{
    class NAModulator : IWaveProvider
    {
        IModulator modulator;
        public WaveFormat WaveFormat { get; set; }

        public NAModulator(IModulator modulator) {
            this.modulator = modulator;
            this.WaveFormat = new WaveFormat(48000, 1);
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            Int16[] buf = new Int16[count/2];
            modulator.GetSamples(buf);
            for (int i = 0; i < count/2; i++) {
                byte[] bytes = BitConverter.GetBytes(buf[i]);
                buffer[2 * i] = bytes[0];
                buffer[2 * i+1] = bytes[1];
            }
            return count;
        }
    }
}
