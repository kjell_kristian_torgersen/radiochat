﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioChat
{
    public delegate void ByteReceived(byte b);

    public delegate void CarrierDetected(bool carrier);

    public class UARTDecoder : IUARTDecoder
    {
        int state;
        int counter;
        int symbolLength;
        public Int64 Threshold { get; set; }
        int rxbits;
        int rxword;

        public event ByteReceived ByteReceived;
        public event CarrierDetected CarrierDetected;
        bool carrierdetected = true;

        public UARTDecoder(int symbollen, Int64 threshold) {
            state = 0;
            counter = 0;
            this.symbolLength = symbollen;
            this.Threshold = threshold;
        }

        private void UpdateCarrierDetected(bool carrier) {
            if (carrier != carrierdetected) {
                carrierdetected = carrier;
                CarrierDetected?.Invoke(carrier);
            }
        }
        public void Decode(Int64[] signal)
        {
            foreach (Int64 sample in signal)
            {
                switch (state)
                {
                    case 0: // Vent på startbit
                        if (sample < -Threshold)
                        { // Startbit passerer terskel, hopp til neste tilstand for å validere
                            state = 1;
                            counter = 1;
                        }
                        else if (sample > Threshold)
                        {
                            UpdateCarrierDetected(true);
                        }
                        else {
                            UpdateCarrierDetected(false);
                        }
                        break;
                    case 1: // Validering av startbit, sjekk at vi har halve startbitet i nærheten av 0 eller lavere.
                        counter++;
                        if (counter >= symbolLength / 2)
                        { // Start bit OK. Hopp til neste tilstand for å sample databit
                            rxbits = 0; // Nullstill variabler
                            counter = 1;
                            rxword = 0;
                            state = 2;
                        }
                        if (sample > Threshold)
                        { // Start bitet var ikke godt nok. Hopp tilbake til tilstanden for å vente på nytt startbit
                            state = 0;
                        }
                        break;
                    case 2: // Samle databittene
                        counter++; // Tell opp en symbol periode. Nå startet vi midt i forrige bit, å vil ende i midten av neste bit
                        if (counter >= symbolLength)
                        {
                            counter = 0; // Nullstill teller å lagre unna mottatt bit
                            rxword >>= 1; // Lag plass til neste bit
                            rxword |= (sample < 0) ? 0 : 0x200; // Legg til bit dersom 1
                            rxbits++; // Tell antall mottatte bit
                            if (rxbits >= 9)
                            { // Dersom 9 bit, valider stoppbit
                                if ((rxword & 0x200) != 0)
                                { // stoppbit er ok
                                    rxword >>= 1; // fjern stoppbit
                                    ByteReceived?.Invoke((byte)rxword);
                                    state = 0; // ferdig, vent på neste startbit
                                }
                                else
                                {
                                    // ikke gyldig stoppbit,
                                    state = 0; // ferdig, vent på neste startbit
                                }
                            }
                        }
                        break;
                    default:
                        state = 0;
                        break;
                }
            }
        }
    }
}
