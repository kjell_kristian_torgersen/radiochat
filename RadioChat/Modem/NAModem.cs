﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RadioChat
{
    class NAModem
    {
        private WaveOut player;
        private WaveIn recorder;

        BinaryWriter samples = new BinaryWriter(File.Open("samples.bin", FileMode.Create));
        BinaryWriter demod = new BinaryWriter(File.Open("uart.bin", FileMode.Create));

        IModulator modulator;
        NAModulator nam;
        IDemodulator dbpsk;
        IUARTDecoder uart;

        public bool Transmit { get { return modulator.IsTransmitting; } set { modulator.IsTransmitting = value; } }
        public event TransmissionDone TransmissionDone { add { modulator.TransmissionDone += value; } remove { modulator.TransmissionDone -= value; } }
        public event ByteReceived ByteReceived { add { uart.ByteReceived += value; } remove { uart.ByteReceived -= value; } }
        public event CarrierDetected CarrierDetected { add { uart.CarrierDetected += value; } remove { uart.CarrierDetected -= value; } }

        public NAModem(int symlen, int freq, int threshold)
        {
            modulator = new DBPSKMod(symlen, freq);
            dbpsk = new DBPSKDemod(symlen);
            uart = new UARTDecoder(symlen, threshold);

            recorder = new WaveIn();
            recorder.WaveFormat = new WaveFormat(48000, 1);
            recorder.DataAvailable += Recorder_DataAvailable;
            nam = new NAModulator(modulator);

            // set up playback
            player = new WaveOut();
            player.Init(nam);

            // begin playback & record
            player.Play();
            recorder.StartRecording();
        }

        public void SendByte(byte b)
        {
            modulator.AddByte(b);
        }

        public void SendBytes(byte[] arr)
        {
            modulator.AddBytes(arr);
        }

        public void Close()
        {
            recorder.StopRecording();
            player.Stop();
            samples.Close();
            demod.Close();
        }

        private void Recorder_DataAvailable(object sender, WaveInEventArgs e)
        {
            int[] signal = new int[e.BytesRecorded / 2];
            for (int i = 0; i < e.BytesRecorded / 2; i++)
            {
                Int16 val = BitConverter.ToInt16(e.Buffer, 2 * i);
                signal[i] = val;
                samples.Write(val);
            }
            var dem = dbpsk.Demod(signal);
            foreach (var s in dem)
            {
                demod.Write((int)(s));
            }
            uart.Decode(dem);
        }

        internal void Clear()
        {
            modulator.Clear();
        }
    }
}
