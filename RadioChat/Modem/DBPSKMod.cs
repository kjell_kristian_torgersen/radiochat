﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioChat
{
    class DBPSKMod : IModulator
    { 
        int symlen;
       
        public bool IsTransmitting { get; set; }

        private bool currentBit = false;
        bool done = true;
        private Queue<byte> txqueue = new Queue<byte>();
        private int freq;

        int sw = 0;
        int tx_word = 0xFFFF;
        int tx_idx = 0;

        public event TransmissionDone TransmissionDone;

        public DBPSKMod(int symlen, int freq)
        {
            this.symlen = symlen;
            this.freq = freq;
        }

        public void AddByte(byte b)
        {
            done = false;
            txqueue.Enqueue(b);
        }

        public void AddBytes(byte[] arr)
        {
            done = false;
            foreach (byte b in arr)
            {
                txqueue.Enqueue(b);
            }
        }

        public void Clear()
        {
            txqueue.Clear();
        }

        public void GetSamples(short[] samples)
        {
            if (IsTransmitting)
            {
                for (int i = 0; i < samples.Length; i++)
                {
                    if (currentBit)
                    {
                        samples[i] = ((Int16)(32767.0 * Math.Sin(2 * Math.PI * freq * sw / (double)symlen)));
                    }
                    else
                    {
                        samples[i] = ((Int16)(-32767.0 * Math.Sin(2 * Math.PI * freq * sw / (double)symlen)));
                    }
                    if (sw >= symlen)
                    {
                        // Nullstill å sjekk om vi skal sende en 0. For å sende nuller må vi skifte 180 grader.
                        sw = 0;
                        if ((tx_word & 1) == 0)
                        {
                            currentBit = !currentBit;
                        }
                        // Gjør klart for å plukke neste bit neste gang
                        tx_word >>= 1;

                        // Tell opp hvor mange bit vi har sendt
                        tx_idx++;

                        // Dersom vi har sendt 10 bit, plukk neste karakter dersom den er klar, eller send ett 10 bit ord med høy (standard UART å sende høy dersom en ikke har karakterer)
                        if (tx_idx >= 10)
                        {
                            // Ligger ett nytt ord klart?
                            if (txqueue.Count != 0)
                            { // Ja
                                tx_word = 0xFF00; // Fyll med startbit, lag plass for mottatt byte som skal ut på "lufta"
                                tx_word |= ((txqueue.Dequeue()) & 0xFF); // Legg til byte mottatt fra HW uart
                                tx_word <<= 1; // Lag plass for stoppbit
                                tx_word &= ~0x1; // Stoppbit (denne linjen trengs kanskje ikke ...)
                            }
                            else
                            { // Send bare høy (ren bærebølge)
                                if (done == false)
                                {
                                    done = true;
                                    TransmissionDone?.Invoke();
                                }
                                tx_word = 0xFFFF;
                                //isTransmitting = false;
                            }
                            // Nullstill bit teller
                            tx_idx = 0;
                        }
                    }
                    sw++;
                }
            }
            else
            {
                for (int i = 0; i < samples.Length; i++)
                {
                    samples[i] = 0;
                }
            }
        }
    }
}
