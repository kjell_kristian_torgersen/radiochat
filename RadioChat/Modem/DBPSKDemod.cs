﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioChat
{
    class DBPSKDemod : IDemodulator
    {
        int[] samplebuffer;
        Int64[] accbuffer;
        Int64 acc;
        int idx;
        int idxp;

        public DBPSKDemod(int symlength) {
            samplebuffer = new int[symlength];
            accbuffer = new Int64[symlength];
            idx = 0;
        }

        public Int64[] Demod(int[] samples) {
            Int64[] result = new Int64[samples.Length];
            for (int i = 0; i < samples.Length; i++) {
                int oldSample = samplebuffer[idx];
                samplebuffer[idx] = samples[i];
                acc -= accbuffer[idxp];
                accbuffer[idxp] = oldSample * samples[i];
                acc += accbuffer[idxp];
                result[i] = acc;
                idx++;
                if (idx == samplebuffer.Length) idx = 0;
                idxp++;
                if (idxp == accbuffer.Length) idxp = 0;
            }
            return result;
        }
    }
}
