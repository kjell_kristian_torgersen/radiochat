﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioChat
{
    public delegate void PackageInterpreter(Package package);

    public delegate void Receiving(bool receiving);

    class PackageDecoder
    {
        int packageIndex;
        int packageChecksum;
        byte[] package;
        public event PackageInterpreter PackageInterpreter;
        public event Receiving Receiving;
        bool receiving = false;

        private void UpdateReceiving(bool receiving)
        {
            if (receiving != this.receiving)
            {
                this.receiving = receiving;
                Receiving?.Invoke(receiving);
            }
        }

        public void ProcessPackage(byte b)
        {
            switch (packageIndex)
            {
                case 0:
                    if (b == Package.START_BYTE)
                    {
                        packageIndex = 1;
                        packageChecksum = b;
                        UpdateReceiving(true);

                    }
                    else
                    {
                        UpdateReceiving(false);
                        if (b >= 32 && b < 128)
                        {
                            Console.Write((char)b);
                        }
                        else
                        {
                            Console.Write(" 0x" + b.ToString("X2"));
                        }
                    }
                    break;
                case 1:
                    package = new byte[b + 3];
                    package[0] = (byte)Package.START_BYTE;
                    package[1] = b;
                    packageChecksum += b;
                    packageIndex = 2;
                    break;
                default:
                    package[packageIndex] = b;
                    packageChecksum += b;
                    packageIndex++;
                    if (packageIndex >= package[1] + 3)
                    {
                        packageIndex = 0;
                        while (packageChecksum > 0x100)
                        {
                            packageChecksum = (ushort)((packageChecksum >> 8)
                                    + (packageChecksum & 0xFF));
                        }
                        packageChecksum = (ushort)((~packageChecksum) & 0xFF);
                        if (packageChecksum == 0)
                        {
                            PackageInterpreter?.Invoke(new Package(package));
                            UpdateReceiving(false);
                        }
                        else
                        {

                            Debug.WriteLine("CS fail for package: ");
                            for (int i = 0; i < package.Length; i++)
                            {
                                Debug.Write(package[i].ToString("X2") + " ");
                            }
                            Debug.WriteLine("");


                        }
                    }
                    break;
            }
        }
    }
}
