﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioChat
{
    public class Package
    {
        public static int START_BYTE = 0xAA;

        public PackageType Type { get; set; }
        public byte[] Payload { get; set; } = null;

        public ushort[] PayloadShort {
            get {
                ushort[] ret = new ushort[Payload.Length / 2];
                for (int i = 0; i < Payload.Length / 2; i++)
                {
                    ret[i] = (ushort)(Payload[2 * i] + (Payload[2 * i + 1] << 8));
                }
                return ret;
            }
            set {
                Payload = new byte[value.Length * 2];
                for (int i = 0; i < value.Length; i++)
                {
                    Payload[2 * i] = (byte)(value[i] & 0xFF);
                    Payload[2 * i] = (byte)((value[i] >> 8) & 0xFF);
                }
            }
        }
        public Package(byte[] package)
        {
            Payload = new byte[package[1] - 1];
            Type = (PackageType)package[2];
            for (int i = 0; i < Payload.Length; i++)
            {
                Payload[i] = package[3 + i];
            }
        }

        public Package(PackageType packageType, byte[] payload)
        {
            this.Type = packageType;
            this.Payload = payload;
        }

        public Package(PackageType packageType, ushort[] payloadShort)
        {
            this.Type = packageType;
            this.PayloadShort = payloadShort;
        }

        public static byte getChecksum(byte[] package)
        {
            ushort checksum = 0;
            for (int i = 0; i < package.Length; i++)
            {
                checksum += package[i];
                while (checksum >= 256)
                {
                    checksum -= 255;
                }
            }
            return (byte)((~checksum) & 0xFF);
        }

        public byte[] ToBytes()
        {
            byte[] package = new byte[Payload.Length + 4];

            package[0] = (byte)START_BYTE;
            package[1] = (byte)(Payload.Length + 1);
            package[2] = (byte)Type;
            for (int i = 0; i < Payload.Length; i++)
            {
                package[3 + i] = Payload[i];
            }
            package[3 + Payload.Length] = 0;
            package[3 + Payload.Length] = getChecksum(package);
            return package;
        }

        public override string ToString()
        {

            string info = "TYPE=" + Type.ToString() + " SIZE=" + Payload.Length;
            switch (Type)
            {
                case PackageType.FileBlock:
                    int index = BitConverter.ToUInt16(Payload, 0);
                    info += " index=" + index;
                    break;
                case PackageType.Message:
                    string s = "";
                    foreach (byte b in Payload)
                    {
                        s += (char)b;
                    }
                    info += " MESSAGE='" + s + "'";
                    break;
            }
            return info;
        }

    }
}
