﻿namespace RadioChat
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SendFileButton = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.CallSignTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.UARThreshTextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SetButton = new System.Windows.Forms.Button();
            this.WaveNumeric = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.SymlenNumeric = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.RXTextBox = new System.Windows.Forms.TextBox();
            this.TXTextBox = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WaveNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SymlenNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(778, 33);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(123, 30);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.RXTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.TXTextBox, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 33);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(778, 511);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.SendFileButton);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.CallSignTextBox);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.UARThreshTextbox);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.SetButton);
            this.panel1.Controls.Add(this.WaveNumeric);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.SymlenNumeric);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(134, 470);
            this.panel1.TabIndex = 0;
            // 
            // SendFileButton
            // 
            this.SendFileButton.Location = new System.Drawing.Point(9, 310);
            this.SendFileButton.Name = "SendFileButton";
            this.SendFileButton.Size = new System.Drawing.Size(120, 29);
            this.SendFileButton.TabIndex = 10;
            this.SendFileButton.Text = "Send file";
            this.SendFileButton.UseVisualStyleBackColor = true;
            this.SendFileButton.Click += new System.EventHandler(this.SendFileButton_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(9, 281);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(120, 23);
            this.progressBar1.TabIndex = 9;
            // 
            // CallSignTextBox
            // 
            this.CallSignTextBox.Location = new System.Drawing.Point(9, 249);
            this.CallSignTextBox.Name = "CallSignTextBox";
            this.CallSignTextBox.Size = new System.Drawing.Size(120, 26);
            this.CallSignTextBox.TabIndex = 8;
            this.CallSignTextBox.Text = "LA8DTA";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 226);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Callsign";
            // 
            // UARThreshTextbox
            // 
            this.UARThreshTextbox.Location = new System.Drawing.Point(9, 127);
            this.UARThreshTextbox.Name = "UARThreshTextbox";
            this.UARThreshTextbox.Size = new System.Drawing.Size(120, 26);
            this.UARThreshTextbox.TabIndex = 6;
            this.UARThreshTextbox.Text = "10000000";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "UART threshold";
            // 
            // SetButton
            // 
            this.SetButton.Location = new System.Drawing.Point(9, 159);
            this.SetButton.Name = "SetButton";
            this.SetButton.Size = new System.Drawing.Size(122, 33);
            this.SetButton.TabIndex = 4;
            this.SetButton.Text = "Set";
            this.SetButton.UseVisualStyleBackColor = true;
            this.SetButton.Click += new System.EventHandler(this.SetButton_Click);
            // 
            // WaveNumeric
            // 
            this.WaveNumeric.Location = new System.Drawing.Point(9, 75);
            this.WaveNumeric.Name = "WaveNumeric";
            this.WaveNumeric.Size = new System.Drawing.Size(120, 26);
            this.WaveNumeric.TabIndex = 3;
            this.WaveNumeric.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Waves/Symbol";
            // 
            // SymlenNumeric
            // 
            this.SymlenNumeric.Location = new System.Drawing.Point(9, 23);
            this.SymlenNumeric.Maximum = new decimal(new int[] {
            65536,
            0,
            0,
            0});
            this.SymlenNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SymlenNumeric.Name = "SymlenNumeric";
            this.SymlenNumeric.Size = new System.Drawing.Size(120, 26);
            this.SymlenNumeric.TabIndex = 1;
            this.SymlenNumeric.Value = new decimal(new int[] {
            64,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Symbol length";
            // 
            // RXTextBox
            // 
            this.RXTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RXTextBox.Location = new System.Drawing.Point(143, 3);
            this.RXTextBox.Multiline = true;
            this.RXTextBox.Name = "RXTextBox";
            this.RXTextBox.ReadOnly = true;
            this.RXTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.RXTextBox.Size = new System.Drawing.Size(632, 470);
            this.RXTextBox.TabIndex = 1;
            // 
            // TXTextBox
            // 
            this.TXTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TXTextBox.Location = new System.Drawing.Point(143, 479);
            this.TXTextBox.MaxLength = 240;
            this.TXTextBox.Name = "TXTextBox";
            this.TXTextBox.Size = new System.Drawing.Size(632, 26);
            this.TXTextBox.TabIndex = 2;
            this.TXTextBox.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.TXTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox2_KeyDown);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 544);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.Text = "RadioChat by LA8DTA";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WaveNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SymlenNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button SetButton;
        private System.Windows.Forms.NumericUpDown WaveNumeric;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown SymlenNumeric;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RXTextBox;
        private System.Windows.Forms.TextBox TXTextBox;
        private System.Windows.Forms.TextBox UARThreshTextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CallSignTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button SendFileButton;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}

