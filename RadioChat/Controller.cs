﻿using RadioChat.Modem;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RadioChat
{
    class Controller
    {


        public event TransmissionDone TransmissionDone { add { modem.TransmissionDone += value; } remove { modem.TransmissionDone -= value; } }
        // public event ByteReceived ByteReceived { add { modem.ByteReceived += value; } remove { modem.ByteReceived -= value; } }
        public event CarrierDetected CarrierDetected { add { modem.CarrierDetected += value; } remove { modem.CarrierDetected -= value; } }
        public event PackageInterpreter PackageInterpreter { add { pd.PackageInterpreter += value; } remove { pd.PackageInterpreter -= value; } }
        public event Receiving Receiving { add { pd.Receiving += value; } remove { pd.Receiving -= value; } }

        NAModem modem;
        bool abort = false;
        Thread th;
        PackageDecoder pd;
        FileReceiver fr;
        public bool Busy {
            get {
                return modem.Transmit;
            }
        }
        public void Abort()
        {
            abort = true;
            modem.Clear();
            if(th!=null) th.Join();
        }
        public Controller(int symlen, int waves, int threshold)
        {
            modem = new NAModem(symlen, waves, threshold);
            pd = new PackageDecoder();
            TransmissionDone += Controller_TransmissionDone;
            modem.ByteReceived += Modem_ByteReceived;
            PackageInterpreter += Controller_PackageInterpreter;
            fr = new FileReceiver(this);
        }

        private void Controller_PackageInterpreter(Package package)
        {
            fr.PackageReceived(package); 
        }

        private void Modem_ByteReceived(byte b)
        {
            pd.ProcessPackage(b);
        }

        private void Controller_TransmissionDone()
        {
            th = new Thread(() =>
            {
                Thread.Sleep(500);
                modem.Transmit = false;
            });
            th.Start();
        }

        public void SendMessage(string sender, string message)
        {
            Package p = new Package(PackageType.Message, System.Text.Encoding.ASCII.GetBytes(sender + ": " + message));
            Thread th = new Thread(() =>
            {
                if (modem.Transmit == false)
                {
                    modem.Transmit = true;
                    Thread.Sleep(1000);
                }

                modem.SendBytes(p.ToBytes());

            });
            th.Start();
        }

        public void SendPackage(Package p) {
            modem.SendBytes(p.ToBytes());
        }

        public void SendFile(string filename)
        {
            th = new Thread(() =>
            {
                Package p;
                byte[] payload;
                byte[] bin = File.ReadAllBytes(filename);
                int i;
                if (modem.Transmit == false)
                {
                    modem.Transmit = true;
                    Thread.Sleep(1000);
                }
                
                for (i = 0; i < bin.Length / 192 && !abort; i++)
                {
                    payload = new byte[194];
                    payload[0] = (byte)i;
                    payload[1] = (byte)(i >> 8);
                    for (int j = 0; j < 192; j++)
                    {
                        payload[j + 2] = bin[192 * i + j];
                    }
                   p = new Package(PackageType.FileBlock, payload);
                    modem.SendBytes(p.ToBytes());
                }
                int last = bin.Length % 192;
                payload = new byte[last + 2];
                payload[0] = (byte)i;
                payload[1] = (byte)(i >> 8);
                for (int j = 0; j < last; j++)
                {
                    payload[j + 2] = bin[192 * i + j];
                }
                p = new Package(PackageType.FileBlock, payload);
                modem.SendBytes(p.ToBytes());

            });
            th.Start();
        }

        public void Close()
        {
            Abort();
            modem.Close();
        }
    }
}
