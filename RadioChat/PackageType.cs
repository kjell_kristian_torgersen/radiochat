﻿namespace RadioChat
{
    public enum PackageType { Message=0, ARQMessage=1, MessageACK=2, FileBlock=3, FileSummary=4, FileMissingBlocks=5, FileCompleted=6 };
}