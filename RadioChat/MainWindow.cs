﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RadioChat
{
    public partial class MainWindow : Form
    {

        Controller controller;

        public MainWindow()
        {
            InitializeComponent();
            controller = new Controller((int)SymlenNumeric.Value, (int)WaveNumeric.Value, int.Parse(UARThreshTextbox.Text));
            controller.TransmissionDone += Modem_TransmissionDone;
            controller.PackageInterpreter += Pd_PackageInterpreter;
            controller.CarrierDetected += Modem_CarrierDetected;
            controller.Receiving += Pd_Receiving;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (controller != null) controller.Close();
            Close();
        }

        private void SetButton_Click(object sender, EventArgs e)
        {
            if (controller != null) controller.Close();
            controller = new Controller((int)SymlenNumeric.Value, (int)WaveNumeric.Value, int.Parse(UARThreshTextbox.Text));
            controller.TransmissionDone += Modem_TransmissionDone;
            controller.PackageInterpreter += Pd_PackageInterpreter;
            controller.CarrierDetected += Modem_CarrierDetected;
            controller.Receiving += Pd_Receiving;
        }

        private void Modem_CarrierDetected(bool carrier)
        {
            if (carrier)
            {
                RXTextBox.BackColor = Color.Yellow;
            }
            else
            {
                RXTextBox.BackColor = Color.White;
            }
        }

        private void Pd_Receiving(bool receiving)
        {
            /* if (receiving)
             {
                 textBox1.BackColor = Color.Red;
             }
             else {
                 textBox1.BackColor = Color.White;
             }*/
        }

        private void Pd_PackageInterpreter(Package package)
        {
            if (package.Type == PackageType.Message)
            {
                string s = "";
                foreach (byte b in package.Payload)
                {
                    s += (char)b;
                }
                RXTextBox.AppendText(DateTime.Now.ToShortTimeString() + " RX: " + s + Environment.NewLine);
            }
            else
            {
                RXTextBox.AppendText(DateTime.Now.ToShortTimeString() + " " + package.ToString() + Environment.NewLine);
            }

        }

        private void Modem_TransmissionDone()
        {
            TXTextBox.Enabled = true;
            TXTextBox.Focus();
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (controller != null)
                {
                    string callsign = CallSignTextBox.Text;
                    string message = TXTextBox.Text;
                    RXTextBox.AppendText(DateTime.Now.ToShortTimeString() + " TX: " + callsign + ": " + message + Environment.NewLine);
                    TXTextBox.Text = "";
                    TXTextBox.Enabled = false;
                    controller.SendMessage(callsign, message);
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void SendFileButton_Click(object sender, EventArgs e)
        {
            if (controller.Busy)
            {
                controller.Abort();
            }
            else
            {
                OpenFileDialog ofd = new OpenFileDialog();
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    controller.SendFile(ofd.FileName);
                }
            }
        }
    }
}
