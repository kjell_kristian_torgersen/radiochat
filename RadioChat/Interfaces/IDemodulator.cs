﻿namespace RadioChat
{
    interface IDemodulator
    {
        long[] Demod(int[] samples);
    }
}