﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioChat
{
    public delegate void TransmissionDone();
    public interface IModulator
    {
        event TransmissionDone TransmissionDone;

        bool IsTransmitting { get; set; }
        void AddByte(byte b);
        void AddBytes(byte[] b);
        void Clear();
        void GetSamples(Int16[] samples);
    }
}
