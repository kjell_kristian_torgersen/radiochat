﻿namespace RadioChat
{
    public interface IUARTDecoder
    {
        event ByteReceived ByteReceived;
        event CarrierDetected CarrierDetected;
        void Decode(long[] signal);
    }
}